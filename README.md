# Learning Scala
 
This is a summary of my learning process.
Important sources for this document from the main documentation:
 - [Tour of Scala](https://docs.scala-lang.org/tour/tour-of-scala.html)
 - [Scala Book](https://docs.scala-lang.org/overviews/scala-book/introduction.html)
 - [Overviews](https://docs.scala-lang.org/overviews/)
 - [Cheatsheet](https://docs.scala-lang.org/cheatsheets/index.html)
 - [Visual Scala Reference](https://superruzafa.github.io/visual-scala-reference/).

Interesting pages:
- [Scala Best Practices](https://nrinaudo.github.io/scala-best-practices/)
- [Haoyi's Programming Blog](https://www.lihaoyi.com/)
- [Scala riptutorial](https://riptutorial.com/scala)

# Table of Contents
1. [First program](#first-program )
2. [Variables](#variables)
3. [Control flow](#control-flow)
4. [Classes](#classes)
5. [Scala collections](#scala-collections)
6. [Anonymous functions](#anonymous-functions)
7. [Common sequence methods](#common-sequence-methods)
8. [Testing](#testing)
9. [Functional programming style](#functional-programming-style)
10. [Futures](#futures)


# First program 
 ## Hello World
```java
object Hello {
    def main(args: Array[String]) = {
        println("Hello, World")
    }
}
```
**NOTE**:  There are no semicolons at the end. Scala does not need them

Open the text editor and save the previous code snippet as *Hello.scala*.
Compile it with the following command:
> scalac Hello.scala

Run the code:
> scala Hello

The result is:
> Hello, World

The Java equivalent would be:
```java
public class Hello {
    public static void main(String[] args) {
        System.out.println("Hello, World")
    }
}
```
The program could be written like this:

```java
object HelloWorld extends App {
  println("Hello, World!")
}
```
In this case the program extends **trait** scala.App and that is why the *main* method is not needed.
An object **must** have a *main* method (which will act as entry point) or extend the scala.App trait.

> A trait is like a combination of abstract class and interface in Java.

## Hello World with arguments

```java
object HelloWithArgs {
    def main(args: Array[String]) = {
        if (args.size == 0) {       
           println("Hello, world")
        } else {
          println("Hello, " + args(0))
        }
    }
}
```
Or with **traits**:

```java
object HelloYou extends App {
    if (args.size == 0)
        println("Hello, you")
    else
        println("Hello, " + args(0))
}
```
In this version the trait has the implicit main method and the arguments are accesible though the variable *args*.

Interesting things about *args*:
 - *args.size* and *args.length* will both produce the **same** result.
 - *args* is an object of type `Array`. That is why the access to the elements is done using parenthesis and not `[]`.


# Variables
In Scala variables can be:
 - **inmutable**, use the type `val`. In methods this is the default.
 - **mutable**, use the type `var`.

For example:

```java
val oneName = "Adam Gibson"  // you can not change this.
var anotherName = "Oliver"   // you can change this.
```

The Scala compiler is smart enough to infer the type of the variable, but it is possible to explicitly declare the type after the name if necessary/wanted. For example:

```java
var price: Int = 100 // mutable
```
**NOTE**: Because of readability the **inferred** form -without the type- is **preferred**.

## Variable types
| Name | Value |
|--|--|
|Boolean| `true` or `false` |
|Byte| 8-bit signed two’s complement integer (-2^7 to 2^7-1, inclusive) -128 to 127|
|Short| 16-bit signed two’s complement integer (-2^15 to 2^15-1, inclusive) -32,768 to 32,767|
|Int| 32-bit two’s complement integer (-2^31 to 2^31-1, inclusive) -2,147,483,648 to 2,147,483,647|
|Long| 64-bit two’s complement integer (-2^63 to 2^63-1, inclusive) (-2^63 to 2^63-1, inclusive)|
|Float| 32-bit IEEE 754 single-precision float 1.40129846432481707e-45 to 3.40282346638528860e+38|
|Double| 64-bit IEEE 754 double-precision float 4.94065645841246544e-324d to 1.79769313486231570e+308d|
|Char| 16-bit unsigned Unicode character (0 to 2^16-1, inclusive) 0 to 65,535|
|String| a sequence of `Char`|

 - `BigInt` and `BigDecimal` are also available. Unlike in Java one can use the "normal operations" with those types, even though they are implemented using Java under the hood.
 - Strings are declared using `"` (double quote) while Char are declared using `'` (single quote).

```java
var myChar = 'C'
var myString = "MyString"
```
## Strings
### Concatenation

 ```java
val completeName = firstName + " " + separator + " " + lastName
val completeName2 = s"$firstName $mi $lastName"

println(s"Name: $firstName $mi $lastName")
```

One can include expressions:
```java
println(s"1+1 = ${1+1}")
```

### Multiline
```java
val text = """This is the
|            |text I want to
|            |store in this variable.""".stripMargin
```

Result:
```java
text: String =
This is the
text I want to
store in this variable.
```
# Control flow
## If
```java
if (blablabla) {
    doSomething()
} else if (blebleble) {
    doAntoherThing()
} else {
    doYetAnotherThing()
}
```

`if` expressions always return a result:
```java
val minValue = if (a < b) a else b
```

## For

```java
val nums = Seq(1,2,3)
for (n <- nums) println(n)
```

```java
val items = List(
    "Milk",
    "Bread",
    "Cheese",
    "Tomatoes",
    "Chocolate"
)

for (p <- items) println(p)
```
### Foreach
```Java
items.foreach(println)

items.foreach {
    case(name) => println(name)
}
```

### For expressions
They are/behave like list comprehensions in Python.
For example:

```Java
val nums = Seq(1,2,3)
val doubled = for (n <- nums) yield n * 2
```

## Match
This is like an advanced `switch` in other languages. For example:

```Java
val monthName = i match {
    case 1  => "Monday"
    case 2  => "Tuesday"
    case 3  => "Wednesday"
    case 4  => "Thursday"
    case 5  => "Friday"
    case 6  => "Saturday"
    case 7  => "Sunday"
    case _  => "Invalid day"
}
```
The _ (underscore) represents the *default* value in other programming languages.
Multiple cases can be handled in each case. For example:

```Java
def isTrue(a: Any) = a match {
    case 0 | "" => false
    case _ => true
}
```

*If* expressions can also be used in the *case*:

```Java
val value = count match {
    case 1 => println("You said one.")
    case x if (x > 1 && x < 4) =>
        println("You said two or three.")
    case x if 4 to 44 contains x =>
        println("Something between four and forty-four: " + x)
    case x if x >= 45 => println("You said forty-five or more.")
    case _ => {
        println("You said zero. Or maybe a negative number.")
    }
}
```

OBS:
* Parenthesis in the *if* are not needed, but help to see the complete expression.
* The *case*'s body does not have to be on the same line.
* One can use curly braces {} for the *case*'s body.
* Range can be checked with the *contains* keyword.

## Try/Catch/Finally

```Java
var text = ""
try {
    text = openAndReadAFile(filename)
} catch {
    case ex: FileNotFoundException => println("Couldn't find that file.")
    case ex: IOException => handleException(ex)
} finally {
    // your scala code here, such as closing a database connection
    // or file handle
}
```

# Classes

```Java
class Person(var firstName: String, var lastName: String)
```
This will generate:
* The constructor with 2 parameters.
* Getters and setters for the 2 parameters/class fields.

To make the parameters immutable (*final* in the Java notation), one has to declare them as `val` instead of `var`. For example:
```Java
class Person(val firstName: String, val lastName: String)
```
Creating a person:
```Java
val p = new Person("James", "Bond")
```

## Class constructors
```Java
class Person(var firstName: String, var lastName: String) {

    println("the constructor begins")

    // 'public' access by default
    var age = 0

    // some class fields
    private val HOME = System.getProperty("user.home")

    // some methods
    override def toString(): String = s"$firstName $lastName is $age years old"

    def printHome(): Unit = println(s"HOME = $HOME")
    def printFullName(): Unit = println(this)

    printHome()
    printFullName()
    println("you've reached the end of the constructor")
}
```

Creating a person:

```Bash
scala> var p = new Person("James", "Bond")
the constructor begins
HOME = /home/james
James Bond is 0 years old
you've reached the end of the constructor
var p: Person = James Bond is 0 years old

scala> p
val res1: Person = James Bond is 0 years old

scala> p.age
val res5: Int = 0

scala> p.age = 40
// mutated p.age

scala> p
val res6: Person = James Bond is 40 years old

scala> p.printHome()
HOME = /home/james

scala> p.printFullName
James Bond is 40 years old
```

## Auxiliary class constructors

The primary constructor is automatically generated by Scala based on the parameters.
One can create additional constructors following these rules:
*   The signature of each auxiliary constructor **must be** different.
*   The auxiliary constructor **must call** one of the previously defined constructors.

```Java
// the primary constructor
class Person (var name: String, var age: Int) {

    // one-arg auxiliary constructor
    def this(name: String) = {
        this(name, 0)  // Calling the primary constructor.
    }

    // one-arg auxiliary constructor
    def this(age: Int) = {
        this("No name", age)  // Calling the primary constructor.
    }

    // zero-arg auxiliary constructor
    def this() = {
        this("No name", 0)
    }

    override def toString = s"Person: $name is $age years old"
}
```

Default values for constructor parameters can be given as named parameters:
```Java
class Person(var name: String = "No name", var age: Int = 0)
```

Creating a Person:
```Java
val p = new Person(name="James Bond", age=40)
```

## Methods

```Java
// One liner, without return type.
def triple(input: Int) = input * 3

// One liner, with return type and multiple parameters
def mult(a: Int, b: Int, c: Int): Int = a * b * c

def doubleAndAdd(a: Int, b: Int): Int = {
    val a2 = a * 2
    val b2 = b * 2
    val added = a2 + b2
    added  // Last expression is taken to be the return value.
}
```

## Traits

### Traits can represent interfaces

Lets take a look at an example:

```Java
trait Speaker {
    def speak(): String
}

trait TailWagger {
    def startTail(): Unit
    def stopTail(): Unit
}

trait Runner {
    def startRunning(): Unit
    def stopRunning(): Unit
}
```

As with interfaces in Java, one can extend multiple of them. For example:

```Java
class Dog extends Speaker with TailWagger with Runner {
    // Speaker
    def speak(): String = "Woof!"

    // TailWagger
    def startTail(): Unit = println("tail is wagging")
    def stopTail(): Unit = println("tail is stopped")

    // Runner
    def startRunning(): Unit = println("I'm running")
    def stopRunning(): Unit = println("Stopped running")
}
```

### Traits can represent abstract classes

Lets take a look at an example:

```Java
trait Pet {
    def speak = println("Bla)     // concrete implementation of a speak method
    def comeToMaster(): Unit      // abstract
}

class Dog(name: String) extends Pet {
    def comeToMaster(): Unit = println("Woo-hoo, I'm coming!")
}

class Cat extends Pet {
    // override 'speak'
    override def speak(): Unit = println("meow")
    def comeToMaster(): Unit = println("That's not gonna happen.")
}
```

### Traits can be mixed in on the fly
For example:
```Java
trait TailWagger {
    def startTail(): Unit = println("tail is wagging")
    def stopTail(): Unit = println("tail is stopped")
}

trait Runner {
    def startRunning(): Unit = println("I'm running")
    def stopRunning(): Unit = println("Stopped running")
}

class Dog(name: String)

val d = new Dog("Fido") with TailWagger with Runner
```

The example above works, because the traits have a **default implementation**.
Traist do **not** allow constructor parameters.

## Abstract classes
One usually uses traits instead of scala abstract classes. There are two reasons to use scala abstract classes:
* A base class which requires constructor arguments is needed.
* The code written in Scala will be used/called from Java code.

Defining an **abstract** class:
```Java
abstract class Animal(name: String)
```
Important note:
> One class can only extend one abstract class.


Creating an abstract class and extending it:
```Java
abstract class Pet (name: String) {
    def speak(): Unit = println("Bla")  // concrete implementation
    def comeToMaster(): Unit            // abstract method
}

class Dog(name: String) extends Pet(name) {
    override def speak() = println("Woof")
    def comeToMaster() = println("Here I come!")
}
```

## Companion objects
A _companion object_ in Scala is an `object` that’s declared in the same file as a `class`, and has the same name as the class. A companion object and its class can access each other’s **private members** (fields and methods).

```Java
class Person {
    var name = ""
}

object Person {
    def apply(name: String): Person = {
        var p = new Person
        p.name = name
        p
    }
    def unapply(p: Person): String = s"${p.name}"
}
```
One can now create a person in different ways:
> val p1 = new Person("David Attenborough")

Or
> val p2 = Person("Marco Polo")

Which is syntactic sugar for this:
> val p2 = Person.apply("Marco Polo")

The companion object can have many different `apply` methods, creating different constructors.
The `unapply` method is used to "de-construct" the class. It can return anything (String, or Tuple, for example).

One rarely needs to write an `unapply` method yourself, one gets `apply` and `unapply` methods for free when one creates classes as _case classes_ rather than as the “regular” Scala classes.

## Case classes
A case class has all of the functionality of a regular class, and more. When the compiler sees the `case` keyword in front of a `class`, it generates code for you, with the following benefits:

-   Case class constructor parameters are public `val` fields by default, so accessor methods are generated for each parameter.
-   An `apply` method is created in the companion object of the class, so you don’t need to use the `new` keyword to create a new instance of the class.
-   An `unapply` method is generated, which lets you use case classes in more ways in `match` expressions.
-   A `copy` method is generated in the class. You may not use this feature in Scala/OOP code, but it’s used all the time in Scala/FP.
-   `equals` and `hashCode` methods are generated, which let you compare objects and easily use them as keys in maps.
-   A default `toString` method is generated, which is helpful for debugging.

# Scala collections

| Class | Description |
|--|--|
|`ArrayBuffer`| Indexed, **mutable** sequence |
|`List`       | Linear (linked list), **immutable** sequence |
|`Vector`     | Indexed, **immutable** sequence |
|`Map`        | Base `Map` (key/value pairs) class. Can be mutable or immutable.|
|`Set`        | Base `Set` class. Can be mutable or immutable. |

More information:
* [Performance characteristics](https://docs.scala-lang.org/overviews/collections-2.13/performance-characteristics.html).
* [Concrete mutable collection classes](https://docs.scala-lang.org/overviews/collections-2.13/concrete-mutable-collection-classes.html)
* [Concrete immutable collection classes](https://docs.scala-lang.org/overviews/collections-2.13/concrete-immutable-collection-classes.html)

## ArrayBuffer

The ArrayBuffer is an indexed, **mutable** sequence.
In order to use the ArrayBuffer one has to import it:
```Java
import scala.collection.mutable.ArrayBuffer
```

### Creating an ArrayBuffer
```Java
val ints = ArrayBuffer[Int]()     // Creates the ArrayBuffer.
val nums = ArrayBuffer(1, 2, 3)   // Creates and initializes an ArrayBuffer.
```

### Adding and removing elements
```Java
// Adding elements to the collection.
ints += 1                         // Adds one element.
ints += 2                         // Adds another element.
ints += 3 += 4                    // Adds 2 elements.
ints ++= List(7, 8, 9)            // Adds multiple elements from another collection.

// Removing elements from the collection.
ints -= 9                         // Removes one element.
ints -= 7 -= 8                    // Removes multiple elements.
ints --= Array(5, 6)              // Removes multiple elements using another collection.
```

More examples:

```Java
val a = ArrayBuffer(1, 2, 3)         // ArrayBuffer(1, 2, 3)
a.append(4)                          // ArrayBuffer(1, 2, 3, 4)
a.append(5, 6)                       // ArrayBuffer(1, 2, 3, 4, 5, 6)
a.appendAll(Seq(7,8))                // ArrayBuffer(1, 2, 3, 4, 5, 6, 7, 8)
a.clear                              // ArrayBuffer()

val a = ArrayBuffer(9, 10)           // ArrayBuffer(9, 10)
a.insert(0, 8)                       // ArrayBuffer(8, 9, 10)
a.insertAll(0, Vector(4, 5, 6, 7))   // ArrayBuffer(4, 5, 6, 7, 8, 9, 10)
a.prepend(3)                         // ArrayBuffer(3, 4, 5, 6, 7, 8, 9, 10)
a.prepend(1, 2)                      // ArrayBuffer(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
a.prependAll(Array(0))               // ArrayBuffer(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

val a = ArrayBuffer.range('a', 'h')  // ArrayBuffer(a, b, c, d, e, f, g)
a.remove(0)                          // ArrayBuffer(b, c, d, e, f, g)
a.remove(2, 3)                       // ArrayBuffer(b, c, g)

val a = ArrayBuffer.range('a', 'h')  // ArrayBuffer(a, b, c, d, e, f, g)
a.trimStart(2)                       // ArrayBuffer(c, d, e, f, g)
a.trimEnd(2)                         // ArrayBuffer(c, d, e)
```

## Lists
Linear (linked list), **immutable** sequence. [Official documentation](https://www.scala-lang.org/api/current/scala/collection/immutable/List.html).

### Creating a list
```Java
// Without the type.
val ints = List(3, 2, 1)
val names = List("AB", "CD", "EF")

// With the type.
val ints: List[Int] = List(3, 2, 1)
val names: List[String] = List("AB", "CD", "EF")

// Like in Lisp.
val list = 1 :: 2 :: 3 :: Nil
```

### Prepending to a list/Adding elements to a list.
You can't, because `List` is an **immutable** structure :-)
However one can create a new `List`. For example:

```Java
val a = List(1,2,3)

val b = 0 +: a             // New list with 0 and the existing 'a' list.
val c = List(-1, 0) ++: a  // New list with -1, 0 and the existing 'a' list.
```

Appending elements to a `List` is possible, but it is **not** recommended, because it is a **slow** operation.

> Tip: If prepending and appending elements to an immutable sequence is needed, use `Vector` instead.

Because `List` is a linked-list class, trying to access the n-th element of it will cause a linear iteration, which is slow when the  `List` is large.
If you want to access elements like this, use a `Vector` or `ArrayBuffer` instead.

### Iterating a list
```Java
for (name <- names) println(name)
```

This for loop will work with all sequences in the collection classes (`ArrayBuffer`, `List`, `Seq`, `Vector`).

## Vector

A `Vector` is is an indexed, **immutable** sequence. [Official documentation](https://www.scala-lang.org/api/current/scala/collection/immutable/Vector.html).

### Creating a vector
```Java
val nums = Vector(1, 2, 3, 4, 5)
```

### Appending to a vector.
You can't, because `Vector` is an **immutable** structure :-)
However one can create a new `Vector`. For example:

```Java
val a = Vector(1,2,3)
val b = a :+ 4             // New vector containing vector 'a' and 4.
val c = a ++ Vector(4, 5)  // New vector containing vector 'a' and 4, 5.
```

### Prepending to a vector.
You can't, because `Vector` is an **immutable** structure :-)
However one can create a new `Vector`. For example:

```Java
val b = 0 +: a                // New vector containing 0 and vector 'a'.
val c = Vector(-1, 0) ++: a   // New vector containing -1,0 and vector 'a'.
```
### Iterating a vector
```Java
val numbers = Vector(1,2,3)
for (number <- numbers) println(number)
```

## Map
`Map` as an iterable sequence that consists of pairs of keys and values.
Take a look into the [official documentation](https://docs.scala-lang.org/overviews/collections-2.13/maps.html).

### Creating a map
Example of **immutable** `Map`:

```Java
val countries = Map(
    "D" -> "Germany",
    "F" -> "France",
    "D" -> "Danmark"
)
```

To create a **mutable** map one has to import it first:
```Java
import scala.collection.mutable.Map

val countries = collection.mutable.Map(
    "DE" -> "Germany",
    "F" -> "France",
    "D" -> "Danmark"
)
```

### Adding elements to a map
```
countries += ("NL" -> "Netherlands")                  // adding one element.
countries ++= Map("SE" -> "Sweden", "FI" -> "Suomi")  // adding multiple elements.
```

### Removing elements from a map
```
countries -= "SE"
countries -= ("NL", "F")
countries --= List("D", "DE")
```

### Updating elements from a map
```Java
countries("FI") = "Finland"
```

### Traversing a map
```Java
// for loop.
for ((k,v) <- countries) println(s"key: $k, value: $v")

// match expression.
countries.foreach {
    case(code, name) => println(s"code: $code, name: $name")
}
```

## Set
A `Set` does not allow duplicated elements.
[Official documentation](https://docs.scala-lang.org/overviews/collections-2.13/sets.html).

For mutable `Set` one has to import this:
```Java
import scala.collection.mutable.Set
```

## Creating a set
#### Immutable set
```Java
val set = Set[Int](1,2,3)
```
### Mutable set
```Java
val set = scala.collection.mutable.Set[Int]()
```

### Adding elements to a set
```Java
set += 1
set += 2 += 3
set ++= Vector(4, 5)

set.add(6)                // This will return true if added, false if not.
```

### Removing elements from a set
```
set -= 1
set -= (2, 3)
set --= Array(4,5)

set.remove(2)             // This will return true if removed, false if not.

set.clear()               // Remove all elements.
```

# Anonymous functions

```Java
val ints = List.range(1, 10)

val tripledInts = ints.map(_ * 3)                // Simple anonymous function.
val tripledInts = ints.map(i => i * 3)           // More explicit anonymous function.
val tripledInts = ints.map((i: Int) => i * 3)    // Most explicit anonymous function.
```
Similar to list comprehension in Python:
```Java
val tripledInts = for (i <- ints) yield i * 3
```

More examples:
```Java
val x = ints.filter(_ % 2 == 0)                  // Simple anonymous function.
val x = ints.filter(i => i % 2 == 0)             // More explicit anonymous function.
val x = ints.filter((i: Int) => i % 2 == 0)      // Most explicit anonymous function.
```

# Common sequence methods
The following methods will work on all of the collections “sequence” classes, including `Array`, `ArrayBuffer`, `List`, `Vector`. It is important to note that they will *never mutate* the original collection, they will always return a new collection:
-   `map`
-   `filter`
-   `foreach`
-   `head`
-   `tail`
-   `take`, `takeWhile`
-   `drop`, `dropWhile`
-   `reduce`

More methods can be found in the [official documentation](https://docs.scala-lang.org/overviews/collections-2.13/seqs.html).
One can view how these methods work in the [Visual Scala Reference](https://superruzafa.github.io/visual-scala-reference/).
```Java
// Example lists
val nums = (1 to 100).toList
val countries = List("norway", "germany", "spain", "iceland", "france")
```

## Map
```Java
val triples = nums.map(_ * 3)
val capCountries = countries.map(_.capitalize)
```

Example in [Visual Scala Reference](https://superruzafa.github.io/visual-scala-reference/map/).

## Filter
```Java
val lessThanFive = nums.filter(_ < 5)
val shortCountryNames = countries.filter(_.length <= 5)
```

Example in [Visual Scala Reference](https://superruzafa.github.io/visual-scala-reference/filter/).

## Foreach
`foreach` is used for side-effects like printing.
```Java
countries.foreach(println)
countries.filter(_.length <= 5).foreach(println)
```

Example in [Visual Scala Reference](https://superruzafa.github.io/visual-scala-reference/foreach/).

## Head
Takes the first element of a list or throws an exception (`NoSuchElementException`) when called on an empty collection. Example:
```Java
countries.head  // Returns "norway"
```
Example in [Visual Scala Reference](https://superruzafa.github.io/visual-scala-reference/head/).

## Tail
Returns all the elements that are not the head element or throws an exception (`UnsupportedOperationException`) when called on an empty collection. Example:
```Java
countries.tail  // Returns "germany", "spain", "iceland", "france"
```
Example in [Visual Scala Reference](https://superruzafa.github.io/visual-scala-reference/tail/).

## Take and takewhile
The opposite of `drop` and `dropwhile`. [Visual Scala Reference](https://superruzafa.github.io/visual-scala-reference/takeWhile/).
```Java
scala> countries.take(0)
val res15: List[String] = List()

scala> countries.take(1)
val res16: List[String] = List(norway)

scala> countries.take(2)
val res17: List[String] = List(norway, germany)

scala> countries.take(3)
val res18: List[String] = List(norway, germany, spain)

scala> countries.take(4)
val res19: List[String] = List(norway, germany, spain, iceland)

scala> countries.take(5)
val res20: List[String] = List(norway, germany, spain, iceland, france)

scala> countries.take(20)
val res21: List[String] = List(norway, germany, spain, iceland, france)
```

```Java
scala> nums.takeWhile(_ < 5)
res4: List[Int] = List(1, 2, 3, 4)

scala> countries.takeWhile(_.length < 5)
val res23: List[String] = List(norway)   // "germany" does not fulfill the requirement.
```


## Drop and dropwhile
The opposite of `take` and `takewhile`. [Visual Scala Reference](https://superruzafa.github.io/visual-scala-reference/dropWhile/).

```Java
scala> countries.drop(0)
val res28: List[String] = List(norway, germany, spain, iceland, france)

scala> countries.drop(1)
val res29: List[String] = List(germany, spain, iceland, france)

scala> countries.drop(2)
val res30: List[String] = List(spain, iceland, france)

scala> countries.drop(3)
val res31: List[String] = List(iceland, france)

scala> countries.drop(4)
val res32: List[String] = List(france)

scala> countries.drop(5)
val res33: List[String] = List()

scala> countries.drop(20)
val res34: List[String] = List()
```
```Java
scala> countries.dropWhile(_ != "spain")
val res94: List[String] = List(spain, iceland, france)
```

## Reduce
```Java
scala> nums.reduce(_ + _)
val res97: Int = 5050
```
Example in [Visual Scala Reference](https://superruzafa.github.io/visual-scala-reference/reduce/).

# Testing
A common Scala framework for testing is [ScalaTest](https://www.scalatest.org/). Particularely cool is that one can write tests in [many](https://www.scalatest.org/at_a_glance/FlatSpec) [different](https://www.scalatest.org/at_a_glance/FeatureSpec) [formats](https://www.scalatest.org/at_a_glance/WordSpec). If you are comming from the Java world (JUnit) the format you will be most confortable with is called [FunSuite](https://www.scalatest.org/at_a_glance/FunSuite). The project has a very good documentation with lots of examples.

Example of FunSuite style:
```Java
import org.scalatest.FunSuite
class HelloTests extends FunSuite {
    test("the name is set correctly in constructor") {
        val p = new Person("Barney Rubble")
        assert(p.name == "Barney Rubble")
    }
    test("a Person's name can be changed") {
        val p = new Person("Chad Johnson")
        p.name = "Ochocinco"
        assert(p.name == "Ochocinco")
    }
}
```

Example of FunSpec (BDD) style:
```Java
import org.scalatest.FunSpec
class MathUtilsSpec extends FunSpec {
    describe("MathUtils::double") {
        it("should handle 0 as input") {
            val result = MathUtils.double(0)
            assert(result == 0)
        }
        it("should handle 1") {
            val result = MathUtils.double(1)
            assert(result == 2)
        }
        it("should handle really large integers") (pending)
    }
}
```

# Functional programming style
The functional style mantra is:
>Do **NOT** use `null` values

One way to avoid `null` values is to use Option/Some/None. For example:

## Functions
```Java
def toInt(s: String): Option[Int] = {
    try {
        Some(Integer.parseInt(s.trim)) // wraps the value.
    } catch {
        case e: Exception => None
    }
}
```

Using the `toInt` method:
```Java
toInt(x) match {
    case Some(i) => println(i)
    case None => println("That didn't work.")
}
```

## Classes

```Java
class Address (
    var street1: String,
    var street2: Option[String],
    var city: String,
    var state: String,
    var zip: String
)
```

And instanciate like this:
```
val farAway = new Address(
    "1 Main Street",
    None,
    "Current city",
    "That state",
    "12345"
)

val anotherAddress = new Address(
    "123 Main lane",
    Some("Apt. 3C"),
    "The other city",
    "Another state",
    "6789"
)
```

# Futures
From Scaladoc:
> A Future represents a value which may or may not _currently_ be available, but will be available at some point, or an exception if that value could not be made available.

There is a full example in `src`.

A Future can be defined as:
```Java
def getStockPrice(stockSymbol: String): Future[Double] = Future { ...
```

But also as this:
```Java
def getStockPrice ... = Future.apply { method body here }
```

`Future` begins _immediately_ to run the block of code inside the curly braces — it isn’t like the Java `Thread`, where you create an instance and later call its `start` method.
