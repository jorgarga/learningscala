object HelloWithArgs {
    def main(args: Array[String]) = {
        if (args.size == 0) {       
           println("Hello, world")
        } else {
          println("Hello, " + args(0))
        }
    }
}